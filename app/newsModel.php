<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newsModel extends Model {

    protected $table = 'tb_news';
    protected $fillable = [
        'news_title', 'news_url', 'paperId',
    ];
    public $timestamps = true;

    public function newspaper() {
        return $this->belongsTo('App\NewspaperModel','paperId');
    }

}
