<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class newspaperModel extends Model {

    protected $table = 'tb_newspaper';
    protected $fillable = [
        'newspaperName'
    ];
    public $timestamps = true;

    public function news() {
        return $this->hasMany('App\NewsModel', 'paperId');
    }

}
