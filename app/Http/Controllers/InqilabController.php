<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Inqilab;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;
use App\NewsModel;



class InqilabController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'inqilab';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Inqilab();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'inqilab',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

        $url = "https://www.dailyinqilab.com/";
        $html = HtmlDomParser::file_get_html($url);
        $news = array();
        $part1 = $html->find('div[id=breaking_news]');
        foreach($part1 as $data){
            foreach($data->find('a') as $item){
                $newsTitle[] = trim($item->plaintext);
                $newsLink[] = $item->href;
                
            }  
        }

        $part2 = $html->find('div[class=overview]');
        foreach($part2 as $data){
            foreach($data->find('a') as $item){
                $newsTitle[] = trim($item->plaintext);
                $newsLink[] = $item->href;
                
            }  
        }
        $part4 = $html->find('div[class=item]');
        foreach($part4 as $data){
            foreach($data->find('a') as $item){
                if(trim($item->plaintext)){
                   $newsTitle[] = trim($item->plaintext);
                   $newsLink[] = $item->href;
                }
            }  
        }
        $part3 = $html->find('div[class=post-meta]');
        foreach ($part3 as $data) {
            foreach ($data->find('a') as $item) {
                $Title = trim($item->plaintext);
                if ($Title != "বিস্তারিত" and $Title != "...বিস্তারিত" and $Title != "...আরও" and $Title) {
                    $newsTitle[] = $Title;
                    $newsLink[] = $item->href;
                }
            }
        }
       for($i=1;$i<max(count($newsTitle),count($newsLink));$i++){
		NewsModel::create(['news_title'=>$newsTitle[$i],'news_url'=>$newsLink[$i],'paperId'=>8]);

	}


        $this->data['result']  = NewsModel::where('paperId',8)->get();
		
		// Render into template
		return view('inqilab.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}

		
		$this->data['id'] = $id;
		return view('inqilab.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}
		
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('inqilab.view',$this->data);	
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_inqilab');
			
			$id = $this->model->insertRow($data , $request->input(''));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'inqilab/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'inqilab?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('inqilab/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('inqilab')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('inqilab')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}