<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Jugantor;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\NewsModel;



class JugantorController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'jugantor';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Jugantor();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'jugantor',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

        $url = 'http://www.jugantor.com/';
        $html = HtmlDomParser::file_get_html($url);
        $news = array();
        $part1 = $html->find('li[class=selected]');
        foreach ($part1 as $div) {
            foreach ($div->find('li') as $item) {
                $Title = $item->find('a', 0)->plaintext;
                
                if ($Title) {
                    $newsTitle[] = $Title;
                $newsLink[] = $item->find('a', 0)->href;
                }
            }
        }
        $part2 = $html->find('div[id=hl2]');
        foreach ($part2 as $item) {
            $newsTitle[] = $item->find('a', 0)->plaintext;
            $newsLink[] = $item->find('a', 0)->href;
            
        }
        $part3 = $html->find('div[class=leadmorehl2]');
        foreach ($part3 as $item) {
            $newsTitle[] = $item->find('a', 0)->plaintext;
            $newsLink[] = $item->find('a', 0)->href;
            
        }
        $part4 = $html->find('div[class=singlehl2]');
        foreach ($part4 as $item) {
            $newsTitle[] = $item->find('a', 0)->plaintext;
            $newsLink[] = $item->find('a', 0)->href;
            
        }
        $part5 = $html->find('div[class=cat-box-content]');
        foreach ($part5 as $item) {
            foreach ($item->find('a') as $data) {
                $Title = $data->plaintext;
                
                if ($newsTitle) {
                    $newsTitle[] = $Title;
                $newsLink[] = $data->href;
                }
            }
        }
        $part6 = $html->find('div[class=binohl2]');
        foreach ($part6 as $item) {
            $newsTitle[] = $item->find('a', 0)->plaintext;
            $newsLink[] = $item->find('a', 0)->href;
            
        }

        $part7 = $html->find('div[class=dailymn_inner]');
        foreach ($part7 as $part) {
            foreach ($part->find('div[class=firstdaily_news]') as $item) {
                $newsTitle[] = $item->first_child()->find('a', 0)->plaintext;
                $newsLink[] = $item->first_child()->find('a', 0)->href;
                
            }
            foreach ($part->find('div[class=dailyR_news]') as $item) {
                foreach ($item->find('a') as $data) {
                    $newsTitle[]= $data->plaintext;
                    $newsLink[] = $data->href;
                   
                }
            }
        }
        $part8 = $html->find('div[class=bottom_news]');
        foreach ($part8 as $item) {
            foreach ($item->find('a') as $data) {
                $newsTitle[] = $data->plaintext;
                $newsLink[] = $data->href;
                
            }
        }
        for($i=1;$i<max(count($newsTitle),count($newsLink));$i++){
		NewsModel::create(['news_title'=>$newsTitle[$i],'news_url'=>$newsLink[$i],'paperId'=>19]);

	}


        $this->data['result']  = NewsModel::where('paperId',19)->get();
		
		// Render into template
		return view('jugantor.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}

		
		$this->data['id'] = $id;
		return view('jugantor.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}
		
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('jugantor.view',$this->data);	
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_jugantor');
			
			$id = $this->model->insertRow($data , $request->input(''));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'jugantor/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'jugantor?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('jugantor/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('jugantor')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('jugantor')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}