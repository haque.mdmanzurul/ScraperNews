<?php

namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Prothomalo;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator,
    Input,
    Redirect;
use App\NewsModel;


class ProthomaloController extends Controller {

    protected $layout = "layouts.main";
    protected $data = array();
    public $module = 'prothomalo';
    static $per_page = '10';

    public function __construct() {

        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->model = new Prothomalo();
        $this->info = $this->model->makeInfo($this->module);
        $this->access = $this->model->validAccess($this->info['id']);

        $this->data = array(
            'pageTitle' => $this->info['title'],
            'pageNote' => $this->info['note'],
            'pageModule' => 'prothomalo',
            'return' => self::returnUrl()
        );
    }

    public function getIndex(Request $request) {

        if ($this->access['is_view'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');

        $url = 'http://www.prothom-alo.com/';
        $html = HtmlDomParser::file_get_html($url);
        $ret = $html->find('div[class=each_news]');
        foreach ($ret as $div) {
            $newsTitle[] = $div->find('h2[class=title]', 0)->plaintext;
            $newsLink[] = $url . $div->find('h2[class=title]', 0)->first_child()->href;
            
        }

        for($i=1;$i<max(count($newsTitle),count($newsLink));$i++){
		NewsModel::create(['news_title'=>$newsTitle[$i],'news_url'=>$newsLink[$i],'paperId'=>17]);

	}


        $this->data['result']  = NewsModel::where('paperId',17)->get();
        return view('prothomalo.index', $this->data);
    }

    function getUpdate(Request $request, $id = null) {

        if ($id == '') {
            if ($this->access['is_add'] == 0)
                return Redirect::to('dashboard')->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
        }

        if ($id != '') {
            if ($this->access['is_edit'] == 0)
                return Redirect::to('dashboard')->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
        }

        $this->data['access'] = $this->access;
        return view('prothomalo.form', $this->data);
    }

    public function getShow($id = null) {

        if ($this->access['is_detail'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus', 'error');


        $this->data['access'] = $this->access;
        return view('prothomalo.view', $this->data);
    }

    function postSave(Request $request) {
        
    }

    public function postDelete(Request $request) {

        if ($this->access['is_remove'] == 0)
            return Redirect::to('dashboard')
                            ->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus', 'error');
    }

}
