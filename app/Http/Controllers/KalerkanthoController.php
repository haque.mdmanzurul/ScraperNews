<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Kalerkantho;
use Illuminate\Http\Request;
use Sunra\PhpSimple\HtmlDomParser;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ;
use App\NewsModel;



class KalerkanthoController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'kalerkantho';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Kalerkantho();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'kalerkantho',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$url = "http://www.kalerkantho.com/";
        $html = HtmlDomParser::file_get_html($url);
        $news = array();
        $part1 = $html->find('li[data-category="সর্বশেষ"]');
        foreach ($part1 as $item) {
            foreach ($item->find('a') as $data) {
                $newsTitle[] = $data->plaintext;
                $newsLink[] = $url."".$data->href;
                
            }
        }
        $part2 = $html->find('div[class="home-top-news"]');
        foreach ($part2 as $item) {
            foreach ($item->find('a') as $data) {
                $newsTitle[] = trim($data->plaintext);
                $newsLink[] = $url."".$data->href;
                
            }
        }
        $part3 = $html->find('div[class="carousel2"]');
        foreach ($part3 as $item) {
            foreach ($item->find('a') as $data) {
                $newsTitle[] = trim($data->plaintext);
                $newsLink[] = $url."".$data->href;
                
            }
        }
        $part4 = $html->find('div[class=online_summary]');
        foreach ($part4 as $item) {
            foreach ($item->find('a') as $data) {
                $Title = trim($data->plaintext);
                if ($Title != "[ বিস্তারিত ]") {
                    $newsTitle[] = $Title;
                $newsLink[] = $url."".$data->href;
                }
            }
        }
        $part5 = $html->find('div[class=special_report]');
        foreach ($part5 as $item) {
            foreach ($item->find('a') as $data) {
                $Title = trim($data->plaintext);
                if ($Title != "[ বিস্তারিত ]") {
                    $newsTitle[] = $Title;
               	    $newsLink[] = $url."".$data->href;
                }
            }
        }
        $part6 = $html->find('div[class=bottom_jcl]');
        foreach ($part6 as $item) {
            foreach ($item->find('a') as $data) {
                $Title = trim($data->plaintext);
                if ($Title) {
                $newsTitle[] =$Title;
                $newsLink[] = $url."".$data->href;
                }
            }
        }
       for($i=1;$i<max(count($newsTitle),count($newsLink));$i++){
		NewsModel::create(['news_title'=>$newsTitle[$i],'news_url'=>$newsLink[$i],'paperId'=>20]);

	}


        $this->data['result']  = NewsModel::where('paperId',20)->get();
		
		// Render into template
		return view('kalerkantho.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}

		
		$this->data['id'] = $id;
		return view('kalerkantho.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('tb_news'); 
		}
		
		$this->data['id'] = $id;
		$this->data['access']		= $this->access;
		return view('kalerkantho.view',$this->data);	
	}	

	function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_kalerkantho');
			
			$id = $this->model->insertRow($data , $request->input(''));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'kalerkantho/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'kalerkantho?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('kalerkantho/update/'.$id)->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('id')) >=1)
		{
			$this->model->destroy($request->input('id'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('id'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('kalerkantho')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('kalerkantho')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}			


}