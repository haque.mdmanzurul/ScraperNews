@extends('layouts.app')
@section('content')
<div class="page-content row">
    <!-- Page header -->
    <div class="page-header">
        <div class="page-title">
            <h3> {{ $pageTitle }} <small>{{ $pageNote }}</small></h3>
        </div>

        <ul class="breadcrumb">
            <li><a href="{{ URL::to('dashboard') }}"> Dashboard </a></li>
            <li class="active">{{ $pageTitle }}</li>
        </ul>	  

    </div>

    <div class="page-content-wrapper m-t">	
        <?php
        foreach ($result as $data) {
		echo "<a href='$data->news_url' target='_blank'>$data->news_title</a><br/>";
       }
        ?>
    </div>


@stop