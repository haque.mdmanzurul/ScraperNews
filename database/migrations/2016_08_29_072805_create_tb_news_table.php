<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('paperId')-> unsigned();
            $table->string('news_title',400);
            $table->string('news_url',800);
            $table->timestamps();
            
            $table->foreign('paperId')->references('id')->on('tb_newspaper')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tb_news');
    }
}
